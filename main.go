package main

import "fmt"

func hello() string {
	return "Hello world!"
}

func plus() int {
	return 6
}

func main() {
	helloWorld := hello()
	fmt.Println(helloWorld)
}
